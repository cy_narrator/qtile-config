from libqtile.config import Screen
from qtile_config.qtile_bars import *
from qtile_config.global_variables import chosen_wallpaper, wallpaper_display_mode
from qtile_config.monitor_info import monitors, other_monitors_present
SCREENS = []
for monitor in monitors:
    SCREENS.append(Screen(bottom = bottom_bar, wallpaper=chosen_wallpaper, wallpaper_mode=wallpaper_display_mode, width=monitor[0], height=monitor[1], x=monitor[4], y=monitor[5]))
    ## (width_in_pixels, height_in_pixels, name_of_monitor, is_primary_monitor_true_false, x, y, width_in_mm, height_in_mm)
