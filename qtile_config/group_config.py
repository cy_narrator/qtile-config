from libqtile.command import lazy
from qtile_config.global_variables import mod
from libqtile.config import Group, Match

GROUPS = [
Group("Firefox", matches=[Match(wm_class=["firefox"])]),
Group("Midori", matches=[Match(wm_class=["Midori"])]),
Group("Atom", matches=[Match(wm_class=["Atom"])]),
Group("Terminal", matches=[Match(wm_class=["xterm"])]),
Group("5th Group")


]





"""
Group(Documentation Link: https://docs.qtile.org/en/latest/manual/config/groups.html)

class libqtile.config.Group(name: str, matches: Optional[List[libqtile.config.Match]] = None, exclusive=False, spawn: Optional[Union[str, List[str]]] = None, layout: Optional[str] = None, layouts: Optional[List] = None, persist=True, init=True, layout_opts=None, screen_affinity=None, position=9223372036854775807, label: Optional[str] = None)[source]
Represents a "dynamic" group

These groups can spawn apps, only allow certain Matched windows to be on them, hide when they're not in use, etc. Groups are identified by their name.

Parameters
name: string
the name of this group

matches: default ``None``
list of Match objects whose windows will be assigned to this group

exclusive: boolean
when other apps are started in this group, should we allow them here or not?

spawn: string or list of strings
this will be exec() d when the group is created, you can pass either a program name or a list of programs to exec()

layout: string
the name of default layout for this group (e.g. 'max' or 'stack'). This is the name specified for a particular layout in config.py or if not defined it defaults in general the class name in all lower case.

layouts: list
the group layouts list overriding global layouts. Use this to define a separate list of layouts for this particular group.

persist: boolean
should this group stay alive with no member windows?

init: boolean
is this group alive when qtile starts?

position int
group position

label: string
the display name of the group. Use this to define a display name other than name of the group. If set to None, the display name is set to the name.

-----------------------------------------------------------
-----------------------------------------------------------





"""
