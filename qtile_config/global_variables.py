## These variables change behaviour of Qtile
## Documentation: http://docs.qtile.org/en/latest/manual/config/index.html#configuration-variables
import random
from libqtile.utils import guess_terminal
from qtile_config.random_wallpaper import random_wallpaper

terminal = guess_terminal()

mod = "mod4"
"""Bound to windows or super key in most systems"""

## This one is from DT's config: https://gitlab.com/dwt1/dotfiles/blob/master/.config/qtile/config.py
colors = [["#282c34", "#282c34"], # panel background
          ["#3d3f4b", "#434758"], # background for current screen tab
          ["#ffffff", "#ffffff"], # font color for group names
          ["#ff5555", "#ff5555"], # border line color for current tab
          ["#74438f", "#74438f"], # border line color for 'other tabs' and color for 'odd widgets'
          ["#4f76c7", "#4f76c7"], # color for the 'even widgets'
          ["#e1acff", "#e1acff"], # window name
          ["#ecbbfb", "#ecbbfb"]] # backbround for inactive screens

bar_height = 18

# chosen_wallpaper = give_random_wallpaper("/usr/share/backgrounds/archlinux/")
wallpaper_dirs = ["~/wallpapers/", "/usr/share/backgrounds/archlinux/"]
"""List or tuple of directories where wallpaper is stored, may be one or more"""

chosen_wallpaper = random_wallpaper(random.choice(wallpaper_dirs), 0)

wallpaper_display_mode = "fill"
"""Supported mode: fill and stretch"""

auto_fullscreen = True
"""If a window requests to be fullscreen, it is automatically fullscreened. Set this to false if you only want windows to be fullscreen if you ask them to be."""

follow_mouse_focus = True
"""Controls whether or not focus follows the mouse around as it moves across windows in a layout."""

focus_on_window_activation = 'smart'
"""
Behavior of the _NET_ACTIVATE_WINDOW message sent by applications

urgent: urgent flag is set for the window
focus: automatically focus the window
smart: automatically focus if the window is in the current group
never: never automatically focus any window that requests it
"""

bring_front_click = False
"""When clicked, should the window be brought to the front or not. If this is set to "floating_only", only floating windows will get affected (This sets the X Stack Mode to Above.)"""

auto_minimize = True
"""If things like steam games want to auto-minimize themselves when losing focus, should we respect this or not?"""

cursor_wrap = False
"""If true, the cursor follows the focus as directed by the keyboard, warping to the center of the focused window. When switching focus between screens, If there are no windows in the screen, the cursor will warp to the center of the screen."""

dgroups_key_binder = None
"""
A function which generates group binding hotkeys. It takes a single argument, the DGroups object, and can use that to set up dynamic key bindings.

A sample implementation is available in https://github.com/qtile/qtile/blob/master/libqtile/dgroups.py called simple_key_binder(), which will bind groups to mod+shift+0-10 by default.
"""

dgroups_app_rules = []
"""A list of Rule objects which can send windows to various groups based on matching criteria."""

widget_defaults = {
'font': 'sans',
'fontsize': 12,
'padding': 3
}
"""Default settings for widgets."""

extension_defterminal = guess_terminal()
aults = widget_defaults

reconfigure_screens = False
"""Controls whether or not to automatically reconfigure screens when there are changes in randr output configuration."""

wmname = 'LG3D'
"""
Gasp! We're lying here.
In fact, nobody really uses or cares about this string besides java UI toolkits;
you can see several discussions on the mailing lists, GitHub issues, and other WM documentation that suggest setting this string if your java app doesn't work correctly.
We may as well just lie and say that we're a working one by default.
We choose LG3D to maximize irony:
it is a 3D non-reparenting WM written in java that happens to be on java's whitelist.
"""
