from libqtile import bar
from qtile_config.widget_list import widget_list
from qtile_config.global_variables import bar_height




bottom_bar=bar.Bar(
    widget_list,
    bar_height,
    # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
    # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
)
