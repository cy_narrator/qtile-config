from libqtile import hook, qtile
import os
import subprocess
from qtile_config.keybindings import KEYBINDINGS
from qtile_config.global_variables import *
from qtile_config.qtile_layouts import LAYOUTS
from qtile_config.mouse_config import MOUSE_CONFIG
from qtile_config.group_config import GROUPS
from qtile_config.qtile_screens import SCREENS


cwd = os.path.dirname(os.path.realpath(__file__))
## Get current working directory


@hook.subscribe.startup_once
def startup_once():
    home = os.path.expanduser(f"{cwd}/qtile_config/STARTUP/startup_once.sh")
    subprocess.run(home)

@hook.subscribe.startup
def startup():
    home = os.path.expanduser(f"{cwd}/qtile_config/STARTUP/startup_everytime.sh")
    subprocess.run(home)

## Variables needed by qtile
# Everything from qtile_config/global_variables.py and
keys = KEYBINDINGS
groups = GROUPS
mouse = MOUSE_CONFIG
layouts = LAYOUTS
screens = SCREENS


## Remove unneeded variables from the memory once done
del KEYBINDINGS
del GROUPS
del MOUSE_CONFIG
del LAYOUTS
del SCREENS
