from screeninfo import get_monitors

monitors = []
for m in get_monitors():
    monitors.append((m.width, m.height, m.name, m.is_primary, m.x, m.y, m.width_mm, m.height_mm))
    ## (width_in_pixels, height_in_pixels, name_of_monitor, is_primary_monitor_true_false, x, y, width_in_mm, height_in_mm)

primary_monitor = ()
secondary_monitors = []
for m in monitors:
    if m[3]:
        primary_monitor = m
    else:
        secondary_monitors.append(m)


primary_monitor_resolution = (primary_monitor[0], primary_monitor[1])
other_monitors_present = False
if len(secondary_monitors) > 0:
    other_monitors_present = True

## Make sure that the first monitor is the primary monitor
monitors[0] = primary_monitor
monitors = monitors + secondary_monitors
