from libqtile.config import Key
from libqtile.command import lazy
from qtile_config.global_variables import mod, terminal
from qtile_config.group_config import GROUPS
from qtile_config.monitor_info import monitors

PROGRAM_LAUNCH_KEYBINDINGS = [
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "m", lazy.spawn('midori'), desc="Launch Midori"),
    Key([mod], "a", lazy.spawn('atom --no-sandbox ~/.config/qtile'), desc="Launch config in atom"),
    Key([mod], "f", lazy.spawn('firefox'), desc="Launch Firefox")

]




WINDOW_CONTROL_KEYBINDINGS = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),
]

GROUP_KEYBINDINGS = []
for item_index in range(1,len(GROUPS) + 1):
    GROUP_KEYBINDINGS.append(


    Key(
                [mod],
                str(item_index),
                lazy.group[GROUPS[item_index - 1].name].toscreen(),
                desc="Switch to group"
            ))
    GROUP_KEYBINDINGS.append(
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key(
                [mod, "shift"],
                str(item_index),
                lazy.window.togroup(GROUPS[item_index - 1].name, switch_group=True),
                desc="Switch to & move focused window to group",
            ),
    )


MONITOR_KEYBINDINGS = []
for monitor_id in range(1, len(monitors) + 1):
    # MONITOR_KEYBINDINGS.append(
    # Key([mod, "control"], str(monitor_id), lazy.window.to_screen(monitor_id - 1), desc=f"Switch window to monitor {monitor_id}"),
    # )

    MONITOR_KEYBINDINGS.append(
    Key([mod, "control"], str(monitor_id), lazy.to_screen(monitor_id - 1), desc=f"Switch focus to monitor {monitor_id}")

    )

MISC_KEYBINDINGS = [


    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),

    Key([mod, "control"], "r", lazy.restart(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),


]

KEYBINDINGS = PROGRAM_LAUNCH_KEYBINDINGS + WINDOW_CONTROL_KEYBINDINGS + GROUP_KEYBINDINGS + MISC_KEYBINDINGS + MONITOR_KEYBINDINGS


"""

-----------------------------------------------------------
Key(Documentation Link: Link: https://docs.qtile.org/en/latest/manual/config/keys.html)


class libqtile.config.Key(modifiers: List[str], key: str, *commands, desc: str = '')

Defines a keybinding.

Parameters
modifiers: <should be a list of strings>
A list of modifier specifications.
Modifier specifications are one of: "shift", "lock", "control", "mod1", "mod2", "mod3", "mod4", "mod5".

key:
A key specification, e.g. "a", "Tab", "Return", "space".

commands:
A list of lazy command objects generated with the lazy.lazy helper. If multiple Call objects are specified, they are run in sequence.

desc:
description to be added to the key binding
-----------------------------------------------------------
-----------------------------------------------------------
lazy functions(Documentation Link: https://docs.qtile.org/en/latest/manual/config/lazy.html)

General Functions:

lazy.spawn("application") -> Run the application
lazy.spawncmd() -> Open command prompt on the bar. See prompt widget.
lazy.reload_config() -> Reload the config.
lazy.restart() -> Restart Qtile. In X11, it won't close your windows.
lazy.shutdown() -> Close the whole Qtile
-----------------------------------------------------------

Group Functions:

lazy.next_layout() -> Use next layout on the actual group
lazy.prev_layout() -> Use previous layout on the actual group
lazy.screen.next_group() -> Move to the group on the right
lazy.screen.prev_group() -> Move to the group on the left
lazy.screen.toggle_group() -> Move to the last visited group
lazy.group.next_window() -> Switch window focus to next window in group
lazy.group.prev_window() -> Switch window focus to previous window in group
lazy.group["group_name"].toscreen() -> Move to the group called group_name. Takes an optional toggle parameter (defaults to False). If this group is already on the screen, it does nothing by default; to toggle with the last used group instead, use toggle=True.
lazy.layout.increase_ratio() -> Increase the space for master window at the expense of slave windows
lazy.layout.decrease_ratio() -> Decrease the space for master window in the advantage of slave windows
-----------------------------------------------------------

Window Functions:

lazy.window.kill() -> Close the focused window
lazy.layout.next() -> Switch window focus to other pane(s) of stack
lazy.window.togroup("group_name") -> Move focused window to the group called group_name
lazy.window.toggle_floating() -> Put the focused window to/from floating mode
lazy.window.toggle_fullscreen() -> Put the focused window to/from fullscreen mode


"""
