from libqtile import widget, bar
import importlib
widget_list = [
    widget.GroupBox(
    font="Ubuntu Bold"
    ),
    widget.CurrentLayout(),
    widget.Prompt(),
    # widget.WindowName(),
    widget.Chord(
        chords_colors={
            "launch": ("#ff0000", "#ffffff"),
        },
        name_transform=lambda name: name.upper(),
        width=bar.STRETCH
    ),
    # widget.TextBox("                                             ", name="default"),
    # widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
    widget.Systray(),
    widget.Clock(format="%Y-%m-%d %a %I:%M %p"),


    # widget.QuickExit(),
]
